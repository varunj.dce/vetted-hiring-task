"""
Django settings for vetted project.

Generated by 'django-admin startproject' using Django 1.11.17.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.11/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '7k0o7xx$njcnoc&f)n0ahasvfdu4x)t+)%%32biecd-8&zt)=l'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['localhost', '.vettedtest.online']

# Application definition
SHARED_APPS = (
    'tenant_schemas',
    'company',
    'vetted_admin',

    'django.contrib.contenttypes',

    # everything below here is optional
    'django.contrib.auth',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.admin',
)

PUBLIC_SCHEMA_NAME = 'public'

TENANT_APPS = (
    'django.contrib.contenttypes',
    'django.contrib.auth',

    # your tenant-specific apps
    'employee',
    'team',
)

INSTALLED_APPS = [
    'tenant_schemas',  # mandatory, should always be before any django app
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'company',
    'vetted_admin',
    'employee',
    'team',
]

TENANT_MODEL = 'company.Company'

MIDDLEWARE = [
    'tenant_schemas.middleware.SuspiciousTenantMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]
DEFAULT_FILE_STORAGE = 'tenant_schemas.storage.TenantFileSystemStorage'

ROOT_URLCONF = 'vetted.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.template.context_processors.request',
)

WSGI_APPLICATION = 'vetted.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases
ENV = 'local'

if 'VETTED_API_SERVER_ENV' in os.environ:
    ENV = os.environ['VETTED_API_SERVER_ENV']
    host = os.environ['VETTED_DB_HOST']
    db_user = os.environ['VETTED_DB_USER_NAME']
    db_password = os.environ['VETTED_DB_USER_PASSWORD']
else:
    raise AssertionError(
        'Please add VETTED_API_SERVER_ENV, VETTED_DB_HOST, VETTED_DB_USER_NAME, VETTED_DB_USER_PASSWORD to your environment.')

DATABASES = {
    'default': {
        'ENGINE': 'tenant_schemas.postgresql_backend',
        'HOST': host,
        'PORT': 5432,
        'NAME': 'vetted',
        'USER': db_user,
        'PASSWORD': db_password
    }
}

# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

DATABASE_ROUTERS = (
    'tenant_schemas.routers.TenantSyncRouter',
)

# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static/')

VETTED_AUTH = {
    'admin': {
        'session_store': 'vetted_admin.models.AdminSession',
        'user_model': 'vetted_admin.models.Admin',
        'session_duration': 100,
        'extend_session_on_request': False
    },
    'manager': {
        'session_store': 'employee.models.EmployeeSession',
        'user_model': 'employee.models.Employee',
        'session_duration': 100,
        'extend_session_on_request': False
    },
    'employee': {
        'session_store': 'employee.models.EmployeeSession',
        'user_model': 'employee.models.Employee',
        'session_duration': 100,
        'extend_session_on_request': False
    }
}
