from rest_framework import serializers

from ..models import Company


class CompanyDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        exclude = ('created_on', 'updated_on', 'is_deleted')


class CompanyCreationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        exclude = ('created_on', 'updated_on', 'is_deleted', 'domain_url')

    def create(self, validated_data):
        company = Company(**validated_data)
        company.save()
        return company


class CompanyDeletionSerializer(serializers.Serializer):
    company_id = serializers.IntegerField(required=True)

    def delete_company(self):
        data = self.validated_data
        company = Company.objects.filter(id=data['company_id']).first()
        if not company:
            return False, 'Request failed because no company was found for the given ID.'
        else:
            company.is_deleted = True
            company.save()
            return True, 'Request was successful.'
