# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Create your models here.
from django.db import models
from tenant_schemas.models import TenantMixin
from tenant_schemas.signals import post_schema_sync

from employee.models import Employee
from employee.models import Employee


class Company(TenantMixin):
    name = models.CharField(max_length=100)
    website = models.URLField(max_length=100)
    address = models.CharField(max_length=1024)
    work_category = models.CharField(max_length=100)
    admin_email = models.EmailField(max_length=100)
    contact = models.CharField(max_length=100)
    sub_domain = models.CharField(max_length=100, unique=True)

    created_on = models.DateField(auto_now_add=True)
    updated_on = models.DateField(auto_now=True)
    is_deleted = models.BooleanField(default=False)

    # default true, schema will be automatically created and synced when it is saved
    auto_create_schema = True


def create_company_manager(sender, tenant, **kwargs):
    pass


post_schema_sync.connect(create_company_manager, sender=Company)
