from django.urls import path

from .views import AdminSignupView, AdminLoginView, AdminCompanyView

urlpatterns = [
    path('native/signup/', AdminSignupView.as_view(), name='native_signup'),
    path('native/login/', AdminLoginView.as_view(), name='native_login'),
    path('company/list/', AdminCompanyView.as_view(), name='list_companies'),
    path('company/create/', AdminCompanyView.as_view(), name='create_company'),
    path('company/delete/', AdminCompanyView.as_view(), name='delete_company'),
]
