from django.contrib.auth.hashers import check_password
from django.contrib.sites.shortcuts import get_current_site
from rest_framework.views import APIView
from tenant_schemas.utils import schema_context

from company.api.serializers import CompanyDetailsSerializer, CompanyCreationSerializer, CompanyDeletionSerializer
from company.models import Company
from employee.api.serializers import EmployeeCreationSerializer
from vetted.settings import VETTED_AUTH
from vetted_django.auth_service.admin_auth_service import AdminSessionService
from vetted_django.middleware.auth import AdminAuthToken
from vetted_django.response_codes import RESPONSE_CODES
from vetted_django.restful_response import send_response
from .serializers import AdminSignupSerializer, AdminDeviceSerializer, AdminLoginSerializer
from ..models import Admin, AdminDevice, AdminSession
from django.db import transaction
admin_session_service = AdminSessionService(AdminSession, VETTED_AUTH['admin']['session_duration'])


# Create your views here.
class AdminSignupView(APIView):

    @transaction.atomic
    def post(self, request):
        response = dict()
        admin_data_serializer = AdminSignupSerializer(data=request.data.get('admin'))

        if admin_data_serializer.is_valid():
            admin_data = admin_data_serializer.validated_data

            # Check if a user is already associated with the email ID
            if Admin.objects.filter(email=admin_data['email'], is_deleted=False).first():
                return send_response(response_code=RESPONSE_CODES['FAILURE'],
                                     developer_message='Request failed because admin with email already exists.',
                                     ui_message='Admin with email %s already exists. Please try to login or use a different email to signup.' %
                                                admin_data[
                                                    'email'])

            admin = admin_data_serializer.save()
            admin_device_serializer = AdminDeviceSerializer(data=request.data.get('device'))
            if admin_device_serializer.is_valid():
                admin_device_data = admin_device_serializer.validated_data

                admin_device = AdminDevice.objects.filter(admin=admin, device_id=admin_device_data['device_id']).first()
                if not admin_device:
                    admin_device = admin_device_serializer.save(admin=admin)
                response['session_token'] = admin_session_service.create(admin, admin_device)
                response['admin'] = admin_data

                return send_response(response_code=RESPONSE_CODES['SUCCESS'], data=response,
                                     developer_message='Request was successful.')

            else:
                return send_response(response_code=RESPONSE_CODES['FAILURE'],
                                     developer_message='Request failed because of invalid device data.',
                                     error=admin_device_serializer.errors)
        else:
            return send_response(response_code=RESPONSE_CODES['FAILURE'],
                                 developer_message='Request failed because of invalid admin data.',
                                 error=admin_data_serializer.errors)


class AdminLoginView(APIView):
    def post(self, request):
        response = dict()
        admin_login_serializer = AdminLoginSerializer(data=request.data.get('login_credentials'))
        if admin_login_serializer.is_valid():
            admin_credentials = admin_login_serializer.validated_data
            admin = Admin.objects.filter(email=admin_credentials['email']).first()

            if not admin:
                return send_response(response_code=RESPONSE_CODES['LOGIN_FAILURE'],
                                     ui_message='Sorry we could not find a match for your username and password',
                                     developer_message='Request failed because admin does not exist for given email.')

            if check_password(admin_credentials['password'], admin.password):
                admin_device_serializer = AdminDeviceSerializer(data=request.data['user_device'])
                if admin_device_serializer.is_valid():
                    admin_device_data = admin_device_serializer.validated_data
                    admin_device = AdminDevice.objects.filter(admin=admin, device_id=admin_device_data['device_id'],
                                                              is_deleted=False).first()

                    if not admin_device:
                        admin_device = admin_device_serializer.save(admin=admin)
                    response['session_token'] = admin_session_service.create(admin, admin_device)
                    response['admin'] = AdminSignupSerializer(instance=admin).data

                    return send_response(response_code=RESPONSE_CODES['SUCCESS'], data=response,
                                         developer_message='Request was successful.')
                else:
                    return send_response(response_code=RESPONSE_CODES['FAILURE'], error=admin_device_serializer.errors,
                                         developer_message='Request failed because of invalid device data.')
            else:
                return send_response(response_code=RESPONSE_CODES['LOGIN_FAILURE'],
                                     ui_message='Sorry we could not find a match for your username and password',
                                     developer_message='Request failed because the password entered was invalid.')
        else:
            return send_response(response_code=RESPONSE_CODES['FAILURE'], error=admin_login_serializer.errors,
                                 developer_message='Request failed because of invalid login credentials.')


class AdminCompanyView(APIView):
    permission_classes = (AdminAuthToken,)

    # List all companies
    def get(self, request):
        company_list = Company.objects.filter(is_deleted=False).all()
        company_list = CompanyDetailsSerializer(company_list, many=True).data
        return send_response(response_code=RESPONSE_CODES['SUCCESS'], developer_message='Request was successful.',
                             data={'company_list': company_list})

    # Create new company
    @transaction.atomic
    def post(self, request):
        company_serializer = CompanyCreationSerializer(data=request.data.get('company_details', None))
        if company_serializer.is_valid():
            company_details = company_serializer.validated_data
            base_url = get_current_site(request).domain
            domain_url = '%s.%s' % (company_details['schema_name'], base_url)
            employee_serializer = EmployeeCreationSerializer(data=request.data.get('manager_details', None))
            if employee_serializer.is_valid():
                manager_details = employee_serializer.validated_data
                company = company_serializer.save(domain_url=domain_url)
                with schema_context(company_details['schema_name']):
                    manager = employee_serializer.save(is_manager=True)
                return send_response(response_code=RESPONSE_CODES['SUCCESS'],
                                     developer_message='Request was successful.')
            else:
                return send_response(response_code=RESPONSE_CODES['FAILURE'], error=employee_serializer.errors,
                                     developer_message='Request failed because of invalid manager details.')
        else:
            return send_response(response_code=RESPONSE_CODES['FAILURE'], error=company_serializer.errors,
                                 developer_message='Request failed because of invalid company details.')

    # Delete an existing company
    def delete(self, request):
        company_serializer = CompanyDeletionSerializer(data=request.data)
        if company_serializer.is_valid():
            company_details = company_serializer.validated_data
            is_deleted, message = company_serializer.delete_company()
            if is_deleted:
                return send_response(response_code=RESPONSE_CODES['SUCCESS'], developer_message=message)
            else:
                return send_response(response_code=RESPONSE_CODES['FAILURE'], developer_message=message)
        else:
            return send_response(response_code=RESPONSE_CODES['FAILURE'],
                                 developer_message='Request failed because of invalid company id.',
                                 error=company_serializer.errors)
