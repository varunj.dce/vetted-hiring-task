from django.contrib.auth.hashers import make_password
from rest_framework import serializers

from ..models import Admin, AdminDevice


class AdminSignupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Admin
        exclude = ('created_on', 'updated_on', 'is_deleted')

    def create(self, validated_data):
        admin = Admin(**validated_data)
        admin.password = make_password(validated_data.get('password'))
        admin.save()
        return admin


class AdminDeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdminDevice
        exclude = ('created_on', 'updated_on', 'is_deleted', 'admin')

    def create(self, validated_data):
        device = AdminDevice(**validated_data)
        device.save()
        return device

class AdminLoginSerializer(serializers.Serializer):
    email = serializers.CharField(required=True)
    password = serializers.CharField(required=True)

