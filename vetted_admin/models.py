# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from vetted_django.models import TimeStampedModel


# Create your models here.
class Admin(TimeStampedModel):
    email = models.EmailField(max_length=100, unique=True)
    password = models.CharField(max_length=255)


class AdminDevice(TimeStampedModel):
    admin = models.ForeignKey(Admin, on_delete=models.CASCADE, related_name='device_admin')
    device_id = models.CharField(max_length=32)
    device_type = models.CharField(max_length=32)
    fcm_id = models.CharField(max_length=255)


class AdminSession(TimeStampedModel):
    admin = models.ForeignKey(Admin, on_delete=models.CASCADE, related_name='session_admin')
    device = models.ForeignKey(AdminDevice, on_delete=models.CASCADE)
    session_token = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    last_accessed_on = models.DateTimeField(auto_now=True)
    logged_out_on = models.DateTimeField(null=True)
