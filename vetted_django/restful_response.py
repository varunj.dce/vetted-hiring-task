from django.http import HttpResponse
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """

    def __init__(self, data, search=None, **kwargs):

        # In case of search we don't need to json render
        if search:
            content = data
        else:
            content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


# class CamelCaseResponse(Response):
#     def __init__(self, data, status=status.HTTP_200_OK):
#         from django_fiblabs.utilities.restful import camelize
#         super(CamelCaseResponse, self).__init__(data=camelize(data), status=status)


VALIDATION_ERROR_MESSAGE = {"result": False, "message": "Validation error occurred!", "errors": []}

DUPLICATE_RECORD = {"result": False, "message": "Data supplied by you violates the unique constraint", "errors": []}

OBJECT_DOES_NOT_EXIST = {"result": False, "message": "Object does not exist!"}

SUCCESS_MESSAGE = {"result": True}

ERROR_MESSAGE = {"result": False}

EXCEPTION_ERROR_MESSAGE = {"is_successful": False, "error_type": "",
                           "message": "Seems like something went wrong with the API. Please contact the developer"
                           }

UNAUTHORIZED = {'result': False, 'details': 'The user is not authorized to perform this action'}


def send_response(response_code, data=dict(), error=dict(), ui_message=None, developer_message=None):
    if response_code == 90 and not error:
        import sys, traceback
        type_, value_, traceback_ = sys.exc_info()

        error = traceback.format_exception(type_, value_, traceback_)

    return Response({'data': data, 'error': error, 'ui_message': ui_message, 'developer_message': developer_message,
                     'response_code': response_code})
