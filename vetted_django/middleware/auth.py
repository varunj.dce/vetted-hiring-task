from datetime import datetime, timedelta, timezone

from django.conf import settings
from rest_framework.permissions import BasePermission

from vetted_django.utilities.dynamic_loading import string_import


def get_session_details(request):
    headers = request.META
    if not hasattr(settings, 'VETTED_AUTH'):
        raise AttributeError("Add VETTED_AUTH to your django configuration")

    if 'HTTP_AUTH_TOKEN' not in headers or 'HTTP_USER_TYPE' not in headers:
        return False

    # Get the user config based on header
    user_session_config = settings.VETTED_AUTH[headers['HTTP_USER_TYPE']]

    # Get where the session is being stored for this user
    session_store = user_session_config['session_store']

    # Get the duration of session for this user
    session_duration = user_session_config['session_duration']

    # Get the user model path for this user
    user_model = user_session_config['user_model']

    session_duration_from_now = datetime.now(timezone.utc) - timedelta(days=session_duration)

    SessionStore = string_import(session_store)

    session = SessionStore.objects.filter(session_token=headers['HTTP_AUTH_TOKEN'],
                                          created_on__gte=session_duration_from_now,
                                          is_active=True).first()
    UserClass = string_import(user_model)
    return session, UserClass


class EmployeeAuthToken(BasePermission):
    message = 'Adding customers not allowed.'

    """
    Allows access only to authenticated employees.
    """

    def check_token(self, request):

        session, UserClass = get_session_details(request)

        if session:
            request.user = UserClass.objects.get(pk=session.employee_id)

            '''
            if is_authorized(request, session.user_type, request.user):
                return request.user
            else:
                return False
            '''
            if not request.user.is_manager:
                return request.user

        return False

    def has_permission(self, request, view):
        return self.check_token(request)


class ManagerAuthToken(BasePermission):
    """
    Allows access only to authenticated employees who are managers.
    """

    def check_token(self, request):

        session, UserClass = get_session_details(request)

        if session:
            request.user = UserClass.objects.get(pk=session.employee_id)

            '''
            if is_authorized(request, session.user_type, request.user):
                return request.user
            else:
                return False
            '''
            if request.user.is_manager:
                return request.user

        return False

    def has_permission(self, request, view):
        return self.check_token(request)


class AdminAuthToken(BasePermission):
    """
    Allows access only to SaaS admin.
    """

    def check_token(self, request):

        session, UserClass = get_session_details(request)

        if session:
            request.user = UserClass.objects.get(pk=session.admin_id)

            '''
            if is_authorized(request, session.user_type, request.user):
                return request.user
            else:
                return False
            '''
            if request.user:
                return request.user

        return False

    def has_permission(self, request, view):
        return self.check_token(request)
