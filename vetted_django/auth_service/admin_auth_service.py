import hashlib
import os
from datetime import datetime, timedelta, timezone


class AdminSessionService:
    def __init__(self, session_class, session_validity):
        self.SessionClass = session_class
        self.session_validity = datetime.now(timezone.utc) - timedelta(days=session_validity)

    def create(self, admin, device):
        # Delete all older sessions for this admin
        self.SessionClass.objects.filter(admin=admin, device=device, is_deleted=False).update(is_deleted=True,
                                                                                              is_active=False)

        # Creating new session
        new_session = self.SessionClass(admin=admin, device=device,
                                        session_token=str(hashlib.sha1(os.urandom(128)).hexdigest())[:26])
        new_session.save()

        # Return the session token
        return new_session.session_token

    def validate(self, session_token):

        existing_session = self.SessionClass.objects.filter(session_token=session_token, is_deleted=False).first()

        if existing_session:
            if existing_session.last_accessed_on >= self.session_validity:
                existing_session.last_accessed_on = datetime.now()
                existing_session.save()

                return {
                    "is_expired": False,
                    "admin": existing_session.admin
                }
            else:
                return {
                    "is_expired": True,
                    "admin": existing_session.admin
                }
        else:
            return {
                "is_expired": True,
                "admin": None
            }

    def get_valid_sessions_for_admin(self, admin):

        existing_sessions = self.SessionClass.objects.filter(admin=admin, is_deleted=False)

        valid_sessions = []

        for existing_session in existing_sessions:
            if existing_session.last_accessed_on >= self.session_validity:
                valid_sessions.append(existing_session)

        return valid_sessions

    def delete(self, session_token):
        pass
