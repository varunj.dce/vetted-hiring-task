import os
import hashlib
from datetime import datetime, timedelta, timezone


class SessionService:
    def __init__(self, session_class, session_validity):
        self.SessionClass = session_class
        self.session_validity = datetime.now(timezone.utc) - timedelta(days=session_validity)

    def create(self, employee, device):
        # Delete all older sessions for this device-employee combo
        self.SessionClass.objects.filter(employee=employee, device=device, is_deleted=False).update(is_deleted=True,
                                                                                                    is_active=False)

        # Creating new session
        new_session = self.SessionClass(employee=employee, device=device,
                                        session_token=str(hashlib.sha1(os.urandom(128)).hexdigest())[:26])
        new_session.save()

        # Return the session token
        return new_session.session_token

    def validate(self, session_token):

        existing_session = self.SessionClass.objects.filter(session_token=session_token, is_deleted=False).first()

        if existing_session:
            if existing_session.last_accessed_on >= self.session_validity:
                existing_session.last_accessed_on = datetime.now()
                existing_session.save()

                return {
                    "is_expired": False,
                    "employee": existing_session.employee
                }
            else:
                return {
                    "is_expired": True,
                    "employee": existing_session.employee
                }
        else:
            return {
                "is_expired": True,
                "employee": None
            }

    def get_valid_sessions_for_user(self, employee):

        existing_sessions = self.SessionClass.objects.filter(employee=employee, is_deleted=False)

        valid_sessions = []

        for existing_session in existing_sessions:
            if existing_session.last_accessed_on >= self.session_validity:
                valid_sessions.append(existing_session)

        return valid_sessions

    def get_devices_with_valid_sessions_for_user(self, user):

        valid_sessions = self.get_valid_sessions_for_user(user)

        valid_devices = []

        for valid_session in valid_sessions:
            valid_devices.append(valid_session.device)

        return valid_devices

    def delete(self, session_token):
        pass
