# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from vetted_django.models import TimeStampedModel


# Create your models here.

class Team(TimeStampedModel):
    name = models.CharField(max_length=100)
