from django.urls import path

from .views import EmployeeView, EmployeeManagerView, EmployeeLoginView

urlpatterns = [
    path('self/details/', EmployeeView.as_view(), name='native_signup'),
    path('list/', EmployeeManagerView.as_view(), name='list_employee'),
    path('create/', EmployeeManagerView.as_view(), name='create_employee'),
    path('delete/', EmployeeManagerView.as_view(), name='delete_employee'),
    path('login/', EmployeeLoginView.as_view(), name='manager_login'),
    path('update/', EmployeeView.as_view(), name='update_employee')
]
