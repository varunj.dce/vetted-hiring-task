from django.contrib.auth.hashers import make_password
from rest_framework import serializers

from ..models import Employee, EmployeeDevice


class EmployeeDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        exclude = ('created_on', 'updated_on', 'is_deleted', 'password')

    def update(self, instance, validated_data):
        if validated_data.get('contact_number', None):
            instance.contact_number = validated_data['contact_number']

        if validated_data.get('company_email', None):
            instance.company_email = validated_data['company_email']

        instance.save()


class EmployeeCreationSerializer(serializers.ModelSerializer):
    team = serializers.IntegerField(required=False)

    class Meta:
        model = Employee
        exclude = ('created_on', 'updated_on', 'is_deleted', 'is_manager', 'password')

    def create(self, validated_data):
        employee = Employee(**validated_data)
        # employee.password = make_password(generate_password())
        # TODO : Switch to auto password generation after creating the mailer
        employee.password = make_password('1234567890')
        employee.save()
        # TODO : Send email with auth credentials to employee
        return employee


class EmployeeDeletionSerializer(serializers.Serializer):
    employee_id = serializers.IntegerField(required=True)

    def delete_employee(self):
        data = self.validated_data
        employee = Employee.objects.filter(id=data['employee_id']).first()
        if not employee:
            return False, 'Request failed because employee does not exist for the given id.'
        else:
            employee.is_deleted = True
            employee.save()
            return True, 'Request was successful.'


class EmployeeDeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmployeeDevice
        exclude = ('created_on', 'updated_on', 'is_deleted', 'employee')

    def create(self, validated_data):
        device = EmployeeDevice(**validated_data)
        device.save()
        return device


class EmployeeLoginSerializer(serializers.Serializer):
    email = serializers.CharField(required=True)
    password = serializers.CharField(required=True)
