from django.contrib.auth.hashers import check_password
from rest_framework.views import APIView

from vetted.settings import VETTED_AUTH
from vetted_django.auth_service.session_service import SessionService
from vetted_django.middleware.auth import EmployeeAuthToken, ManagerAuthToken
from vetted_django.response_codes import RESPONSE_CODES
from vetted_django.restful_response import send_response
from .serializers import EmployeeDetailSerializer, EmployeeCreationSerializer, EmployeeDeletionSerializer, \
    EmployeeDeviceSerializer, EmployeeLoginSerializer
from ..models import Employee, EmployeeDevice, EmployeeSession

manager_session_service = SessionService(EmployeeSession, VETTED_AUTH['manager']['session_duration'])
employee_session_service = SessionService(EmployeeSession, VETTED_AUTH['employee']['session_duration'])


class EmployeeLoginView(APIView):
    def post(self, request):
        response = dict()
        employee_login_serializer = EmployeeLoginSerializer(data=request.data.get('login_credentials'))
        if employee_login_serializer.is_valid():
            employee_credentials = employee_login_serializer.validated_data
            employee = Employee.objects.filter(company_email=employee_credentials['email']).first()

            if not employee:
                return send_response(response_code=RESPONSE_CODES['LOGIN_FAILURE'],
                                     ui_message='Sorry we could not find a match for your username and password',
                                     developer_message='Request failed because employee does not exist for given email.')

            if check_password(employee_credentials['password'], employee.password):
                employee_device_serializer = EmployeeDeviceSerializer(data=request.data['user_device'])
                if employee_device_serializer.is_valid():
                    employee_device_data = employee_device_serializer.validated_data
                    employee_device = EmployeeDevice.objects.filter(employee=employee,
                                                                    device_id=employee_device_data['device_id'],
                                                                    is_deleted=False).first()

                    if not employee_device:
                        employee_device = employee_device_serializer.save(employee=employee)
                    if employee.is_manager:
                        response['session_token'] = manager_session_service.create(employee, employee_device)
                    else:
                        response['session_token'] = employee_session_service.create(employee, employee_device)
                    response['employee'] = EmployeeDetailSerializer(instance=employee).data

                    return send_response(response_code=RESPONSE_CODES['SUCCESS'], data=response,
                                         developer_message='Request was successful.')
                else:
                    return send_response(response_code=RESPONSE_CODES['FAILURE'],
                                         error=employee_device_serializer.errors,
                                         developer_message='Request failed because of invalid device data.')
            else:
                return send_response(response_code=RESPONSE_CODES['LOGIN_FAILURE'],
                                     ui_message='Sorry we could not find a match for your username and password',
                                     developer_message='Request failed because the password entered was invalid.')
        else:
            return send_response(response_code=RESPONSE_CODES['FAILURE'], error=employee_login_serializer.errors,
                                 developer_message='Request failed because of invalid login credentials.')


class EmployeeManagerView(APIView):
    permission_classes = (ManagerAuthToken,)

    # Fetch a list of company employees
    def get(self, request):
        employee_list = Employee.objects.filter(is_deleted=False, is_manager=False).all()
        employee_list = EmployeeDetailSerializer(employee_list, many=True).data
        return send_response(response_code=RESPONSE_CODES['SUCCESS'], data={'employee_list': employee_list},
                             developer_message='Request was successful.')

    # Create a new company employee
    def post(self, request):
        employee_serializer = EmployeeCreationSerializer(data=request.data)
        if employee_serializer.is_valid():
            employee_data = employee_serializer.validated_data
            employee = employee_serializer.save()
            employee_details = EmployeeDetailSerializer(instance=employee).data
            return send_response(response_code=RESPONSE_CODES['SUCCESS'], data={'employee': employee_details},
                                 developer_message='Request was successful.')

    # Remove a company employee
    def delete(self, request):
        employee_serializer = EmployeeDeletionSerializer(data=request.data)
        if employee_serializer.is_valid():
            is_deleted, developer_message = employee_serializer.delete_employee()
            if is_deleted:
                return send_response(response_code=RESPONSE_CODES['SUCCESS'],
                                     developer_message=developer_message)
            else:
                return send_response(response_code=RESPONSE_CODES['FAILURE'], developer_message=developer_message)
        else:
            return send_response(response_code=RESPONSE_CODES['FAILURE'],
                                 developer_message='Request failed because of invalid data.',
                                 error=employee_serializer.errors)


class EmployeeView(APIView):
    permission_classes = (EmployeeAuthToken,)

    # Fetch own details
    def get(self, request):
        employee = request.user
        employee_details = EmployeeDetailSerializer(instance=employee).data
        return send_response(response_code=RESPONSE_CODES['SUCCESS'], data=employee_details,
                             developer_message='Request was successful.')

    # Update own details
    def put(self, request):
        employee = request.user
        data = request.data
        employee_serializer = EmployeeDetailSerializer(employee, data=data)
        employee_serializer.save()
        data = EmployeeDetailSerializer(instance=employee).data
        return send_response(response_code=RESPONSE_CODES['SUCCESS'], developer_message='Request was successful.',
                             data=data)
