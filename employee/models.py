# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from team.models import Team
from vetted_django.models import TimeStampedModel


# Create your models here.
class Employee(TimeStampedModel):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    designation = models.CharField(max_length=100)
    contact_number = models.CharField(max_length=10)
    company_email = models.EmailField(max_length=100)
    password = models.CharField(max_length=255)
    team = models.ForeignKey(Team, on_delete=models.CASCADE, null=True)
    is_manager = models.BooleanField(default=False)


class EmployeeDevice(TimeStampedModel):
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name='device_employee')
    device_id = models.CharField(max_length=32)
    device_type = models.CharField(max_length=32)
    fcm_id = models.CharField(max_length=255)


class EmployeeSession(TimeStampedModel):
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name='session_employee')
    device = models.ForeignKey(EmployeeDevice, on_delete=models.CASCADE)
    session_token = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    logged_out_on = models.DateTimeField(null=True)
    last_accessed_on = models.DateTimeField(auto_now=True)
